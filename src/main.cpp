#include <iostream>
#include <raytracer/scene/scene.h>
#include <raytracer/scene/object/sphere.h>
#include <raytracer/material/diffuse_material.h>
#include <raytracer/scene/object/plane.h>
#include <raytracer/image/image.h>
#include <raytracer/raytracer.h>

int main(int argc, char **argv)
{
    std::shared_ptr<scene> scene = std::make_shared<::scene>();
    std::shared_ptr<camera> camera = std::make_shared<::camera>();
    camera->set_position(math::vector3d(5.0, 1.0, 0.0));
    camera->set_fov(90.0, 90.0);
    camera->set_look_at(math::vector3d(0.0, 1.0, 0.0));
    scene->set_camera(camera);

    //std::shared_ptr<light> light = std::make_shared<::light>(vector3d(3, 5, -4), vector3d(1, 1, 1));
    const std::shared_ptr<light> light1 = std::make_shared<::light>(
            math::vector3d(3, 4, -4), math::vector3d(1, 1, 1));
    scene->add_light(light1);

    const std::shared_ptr<light> light2 = std::make_shared<::light>(
            math::vector3d(5, 4, 2), math::vector3d(1, 1, 1));
    scene->add_light(light2);

    const std::shared_ptr<sphere> sphere = std::make_shared<::sphere>(
            math::vector3d(0.0, 1.0, -1.0), 1);
    //const std::shared_ptr<material> sphere_material = std::make_shared<::constant_material>(vector3d(1.0, 0.4, 0.1));
    const std::shared_ptr<material> sphere_material = std::make_shared<diffuse_surface>(
            math::vector4d(1.0, 0.3, 0.0, 1.0));
    sphere->set_surface(sphere_material);
    scene->add_object(sphere);

    //fix set_surface??
    const std::shared_ptr<::sphere> sphere2 = std::make_shared<::sphere>(
            math::vector3d(0.0, 1.0, 1.5), 1);
    //0.737255 green 0.560784 blue 0.560784
    const std::shared_ptr<material> sphere_material2 = std::make_shared<diffuse_surface>(
            math::vector4d(0.0, 1.0, 0.9, 1.0));
    sphere2->set_surface(sphere_material2);
    scene->add_object(sphere2);

    const std::shared_ptr<plane> plane = std::make_shared<::plane>(
            math::vector3d(0.0, 0.0, 0.0), math::vector3d(0.0, 19.0, 0.0));
    const std::shared_ptr<material> plane_material = std::make_shared<::diffuse_surface>(
            math::vector4d(0.171875, 0.6328125, 0.37109375, 1.0));
    plane->set_surface(plane_material);
    scene->add_object(plane);

    const std::shared_ptr<image> image = std::make_shared<::image>(1920, 1080);

    std::shared_ptr<raytracer> raytracer = std::make_shared<::raytracer>();
    raytracer->set_scene(scene);
    raytracer->render_to_image(image);
    image->save_to_tga("result");
}