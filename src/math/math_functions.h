#pragma once

#include <cmath>
#include <algorithm>

namespace math
{

    auto const PI = 3.14159265358979323846;

    auto const SAFETY_EPS = 0.000000001;

    inline bool solve_quadratic(const double &a, const double &b, const double &c,
                         double &x0, double &x1)
    {
        auto disc = b * b - 4 * a * c;
        if (disc < 0) return false;
        else if (disc == 0)
        {
            x0 = x1 = -0.5 * b / a;
        } else
        {
            double q = (b > 0) ?
                       -0.5 * (b + sqrt(disc)) :
                       -0.5 * (b - sqrt(disc));
            x0 = q / a;
            x1 = c / q;
        }

        return true;
    }

    template<typename T>
    T clamp(T val, T min_val = T(0), T max_val = T(1))
    {
        return std::max(min_val, std::min(max_val, val));
    }

    template<typename T>
    void swap(T &a, T &b)
    {
        T c(std::move(a));
        a = std::move(b);
        b = std::move(c);
    }
}