#pragma once

#include <cmath>

namespace math
{
    template<typename T>
    class vector3
    {
    public:
        static const std::size_t size = 3;

        explicit vector3() : vector_{0}
        {
        }

        explicit vector3(T x, T y, T z) : vector_{x, y, z}
        {
        }

        const T &x() const
        { return vector_[0]; }

        const T &y() const
        { return vector_[1]; }

        const T &z() const
        { return vector_[2]; }

        T &x()
        { return vector_[0]; }

        T &y()
        { return vector_[1]; }

        T &z()
        { return vector_[2]; }

        vector3 operator+(const vector3 &other) const
        {
            return vector3(vector_[0] + other.x(), vector_[1] + other.y(),
                           vector_[2] + other.z());
        }

        vector3 operator-(const vector3 &other) const
        {
            return vector3(vector_[0] - other.x(), vector_[1] - other.y(),
                           vector_[2] - other.z());
        }

        vector3 operator*(const vector3 &other) const
        {
            return vector3(vector_[0] * other.x(), vector_[1] * other.y(),
                           vector_[2] * other.z());
        }

        vector3 operator/(const vector3 &other) const
        {
            return vector3(vector_[0] / other.x(), vector_[1] / other.y(),
                           vector_[2] / other.z());
        }

        vector3 operator+(T scalar) const
        {
            return vector3(vector_[0] + scalar, vector_[1] + scalar,
                           vector_[2] + scalar);
        }

        vector3 operator-(T scalar) const
        {
            return vector3(vector_[0] - scalar, vector_[1] - scalar,
                           vector_[2] - scalar);
        }

        vector3 operator*(T scalar) const
        {
            return vector3(vector_[0] * scalar, vector_[1] * scalar,
                           vector_[2] * scalar);
        }

        vector3 operator/(T scalar) const
        {
            return vector3(vector_[0] / scalar, vector_[1] / scalar,
                           vector_[2] / scalar);
        }

        vector3 operator-() const
        {
            return vector3(-vector_[0], -vector_[1], -vector_[2]);
        }

        vector3 &operator+=(const vector3 &other)
        {
            *this = *this + other;
            return *this;
        }

        vector3 &operator-=(const vector3 &other)
        {
            *this = *this - other;
            return *this;
        }

        vector3 &operator*=(const vector3 &other)
        {
            *this = *this * other;
            return *this;
        }

        vector3 &operator/=(const vector3 &other)
        {
            *this = *this / other;
            return *this;
        }

        vector3 &operator+=(T scalar)
        {
            *this = *this + scalar;
            return *this;
        }

        vector3 &operator-=(T scalar)
        {
            *this = *this - scalar;
            return *this;
        }

        vector3 &operator*=(T scalar)
        {
            *this = *this * scalar;
            return *this;
        }

        vector3 &operator/=(T scalar)
        {
            *this = *this / scalar;
            return *this;
        }

        T &operator[](const unsigned i)
        { return vector_[i]; }

        const T &operator[](const unsigned i) const
        { return vector_[i]; }

        T length() const
        { return sqrt(length_power_of_two()); }

        T length_power_of_two() const
        {
            return vector_[0] * vector_[0] + vector_[1] * vector_[1]
                   + vector_[2] * vector_[2];
        }

        const vector3 &normalize()
        {
            *this *= (T) (1.0 / length());
            return *this;
        }

        T dot(const vector3 &other) const
        {
            return vector_[0] * other.x() + vector_[1] * other.y()
                   + vector_[2] * other.z();
        }

        vector3<T> cross(const vector3 &vector) const
        {
            return vector3<T>(
                    vector_[1] * vector.z() - vector_[2] * vector.y(),
                    vector_[2] * vector.x() - vector_[0] * vector.z(),
                    vector_[0] * vector.y() -
                    vector_[1] * vector.x());
        }

    private:
        T vector_[size];
    };

    typedef vector3<int> vector3i;
    typedef vector3<unsigned int> vector3ui;
    typedef vector3<float> vector3f;
    typedef vector3<double> vector3d;
    typedef vector3<char> vector3c;
    typedef vector3<unsigned char> vector3uc;
    typedef vector3<short> vector3s;
    typedef vector3<unsigned short> vector3us;
}
