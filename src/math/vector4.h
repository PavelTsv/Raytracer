#pragma once


#include <cmath>
#include "vector3.h"

namespace math
{
    template<typename T>
    class vector4
    {
    public:
        static const std::size_t size = 4;

        explicit vector4() : vector_{0}
        {
        }

        explicit vector4(T x, T y, T z, T w) : vector_{x, y, z, w}
        {
        }

        explicit vector4(const vector3 <T> &vector, T w) : vector_(
                {vector.x(), vector.y(), vector.z(), w})
        {
        }

        vector3 <T> get_vector3() const
        {
            return vector3<T>(vector_[0], vector_[1], vector_[2]);
        }

        const T &x() const
        { return vector_[0]; }

        const T &y() const
        { return vector_[1]; }

        const T &z() const
        { return vector_[2]; }

        const T &w() const
        { return vector_[3]; }

        T &x()
        { return vector_[0]; }

        T &y()
        { return vector_[1]; }

        T &z()
        { return vector_[2]; }

        vector4 operator+(const vector4 &other) const
        {
            return vector4(vector_[0] + other.x(), vector_[1] + other.y(),
                           vector_[2] + other.z(), vector_[3] + other.w());
        }

        vector4 operator-(const vector4 &other) const
        {
            return vector4(vector_[0] - other.x(), vector_[1] - other.y(),
                           vector_[2] - other.z(), vector_[3] - other.w());
        }

        vector4 operator*(const vector4 &other) const
        {
            return vector4(vector_[0] * other.x(), vector_[1] * other.y(),
                           vector_[2] * other.z(), vector_[3] * other.w());
        }

        vector4 operator/(const vector4 &other) const
        {
            return vector4(vector_[0] / other.x(), vector_[1] / other.y(),
                           vector_[2] / other.z(), vector_[3] / other.w());
        }

        vector4 operator+(T scalar) const
        {
            return vector4(vector_[0] + scalar, vector_[1] + scalar,
                           vector_[2] + scalar, vector_[3] + scalar);
        }

        vector4 operator-(T scalar) const
        {
            return vector4(vector_[0] - scalar, vector_[1] - scalar,
                           vector_[2] - scalar, vector_[3] - scalar);
        }

        vector4 operator*(T scalar) const
        {
            return vector4(vector_[0] * scalar, vector_[1] * scalar,
                           vector_[2] * scalar, vector_[3] * scalar);
        }

        vector4 operator/(T scalar) const
        {
            return vector4(vector_[0] / scalar, vector_[1] / scalar,
                           vector_[2] / scalar, vector_[3] / scalar);
        }

        vector4 operator-() const
        {
            return vector4(-vector_[0], -vector_[1], -vector_[2],
                           -vector_[3]);
        }

        vector4 &operator+=(const vector4 &other)
        {
            *this = *this + other;
            return *this;
        }

        vector4 &operator-=(const vector4 &other)
        {
            *this = *this - other;
            return *this;
        }

        vector4 &operator*=(const vector4 &other)
        {
            *this = *this * other;
            return *this;
        }

        vector4 &operator/=(const vector4 &other)
        {
            *this = *this / other;
            return *this;
        }

        vector4 &operator+=(T scalar)
        {
            *this = *this + scalar;
            return *this;
        }

        vector4 &operator-=(T scalar)
        {
            *this = *this - scalar;
            return *this;
        }

        vector4 &operator*=(T scalar)
        {
            *this = *this * scalar;
            return *this;
        }

        vector4 &operator/=(T scalar)
        {
            *this = *this / scalar;
            return *this;
        }

        T &operator[](unsigned i)
        { return vector_[i]; }

        const T &operator[](unsigned i) const
        { return vector_[i]; }

        T length() const
        { return (T) sqrt(x() * x() + y() * y() + z() * z() + w() * w()); }

        T length_power_of_two() const
        { return x() * x() + y() * y() + z() * z() + w() * w(); }

        const vector4 &normalize(T *len = nullptr)
        {
            *this *= (T) (1.0 / length());
            return *this;
        }

        T dot(const vector4 &other) const
        {
            return vector_[0] * other.x() + vector_[1] * other.y()
                   + vector_[2] * other.z() + vector_[3] * other.w();
        }

    private:
        T vector_[size];
    };

    typedef vector4<int> vector4i;
    typedef vector4<unsigned int> vector4ui;
    typedef vector4<float> vector4f;
    typedef vector4<double> vector4d;
    typedef vector4<char> vector4c;
    typedef vector4<unsigned char> vector4uc;
    typedef vector4<short> vector4s;
    typedef vector4<unsigned short> vector4us;
}
