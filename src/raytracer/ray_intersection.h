#pragma once

#include <memory>
#include "raytracer/scene/object/object.h"

class object;


class ray_intersection
{
public:
    ray_intersection() : ray_(math::vector3d(0.0, 0.0, 0.0),
                              math::vector3d(0.0, 0.0, 0.0)),
                         object_(nullptr), lambda_(0.0),
                         position_(0.0, 0.0, 0.0), normal_(0.0, 0.0, 0.0)
    {
    }

    ray_intersection(const ray &ray,
                     const std::shared_ptr<const object> object,
                     const double lambda, const math::vector3d &normal)
            : ray_(ray), object_(object), lambda_(lambda),
              position_(ray.point_on_ray(lambda)), normal_(normal)
    {
    }

    double lambda() const
    { return lambda_; }

    const math::vector3d &normal() const
    { return normal_; }

    std::shared_ptr<const object> get_object() const
    { return object_; }

    const math::vector3d &position() const
    { return position_; }

private:
    ray ray_;
    std::shared_ptr<const object> object_;
    double lambda_;
    math::vector3d position_;
    math::vector3d normal_;
};
