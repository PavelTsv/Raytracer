#pragma once

#include "raytracer/scene/scene.h"
#include "raytracer/image/image.h"


class raytracer
{
public:
    explicit raytracer();

    ~raytracer();

    void set_scene(const std::shared_ptr<scene>& scene)
    { scene_ = scene; }

    void render_to_image(const std::shared_ptr<image>& result_image) const
    {
        if (!scene_) return;

        if (!scene_->get_camera()) return;


        camera &camera = *scene_->get_camera().get();
        camera.set_resolution(result_image->width(),
                              result_image->height());

        for (auto y = 0; y < result_image->height(); ++y)
        {
            for (auto x = 0; x < result_image->width(); ++x)
            {
                const auto ray = camera.primary_ray(x, y);
                auto color = this->trace(ray);
                result_image->set_pixel(color, x, y);

            }
        }
    }

    math::vector4d trace(const ray &ray) const;

    math::vector4d shade(const ray_intersection &intersection) const;

private:
    std::shared_ptr<scene> scene_;

};
