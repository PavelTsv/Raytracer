set(scene_src
        scene.h
        scene.cpp)

add_subdirectory(object)
add_subdirectory(light)
add_subdirectory(camera)

add_library(scene_lib ${scene_src})
target_link_libraries(scene_lib object_lib)
target_link_libraries(scene_lib light_lib)
target_link_libraries(scene_lib camera_lib)