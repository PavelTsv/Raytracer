#pragma once

#include <vector>
#include <limits>
#include "raytracer/scene/object/object.h"
#include "raytracer/scene/camera/camera.h"

class scene
{
public:
    scene();

    virtual ~scene();

    void add_object(const std::shared_ptr<object>& object)
    {
        objects_.push_back(object);
    }

    void add_light(const std::shared_ptr<light>& light)
    {
        lights_.push_back(light);
    }

    void set_background_color(const math::vector4d &color)
    {
        background_color_ = color;
    }

    void set_camera(const std::shared_ptr<camera>& camera)
    {
        camera_ = camera;
    }

    const math::vector4d &background_color() const
    {
        return background_color_;
    }

    const std::vector<std::shared_ptr<light>> &lights() const
    {
        return lights_;
    }

    const std::vector<std::shared_ptr<object>> &objects() const
    {
        return objects_;
    }

    std::shared_ptr<camera> get_camera()
    { return camera_; }

    bool
    closest_intersection(const ray &ray, ray_intersection &intersection,
                         const double max_lambda
                         = std::numeric_limits<double>::infinity()) const;


    bool any_intersection(const ray &ray) const;

private:
    math::vector4d background_color_;
    std::shared_ptr<camera> camera_;
    std::vector<std::shared_ptr<object>> objects_;
    std::vector<std::shared_ptr<light>> lights_;
};
