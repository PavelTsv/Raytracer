#include "scene.h"

scene::scene() : background_color_(0.0, 0.0, 0.0, 1.0)
{
}

scene::~scene() = default;

bool scene::closest_intersection(const ray &ray,
                                 ray_intersection &intersection,
                                 const double max_lambda) const
{
    auto closest_lambda = max_lambda;
    ray_intersection temp_intersection;
    ray_intersection closest_intersection;
    auto hit(false);

    for (auto const &object : objects_)
    {
        if (object->intersects(ray, temp_intersection))
        {
            if (temp_intersection.lambda() < closest_lambda)
            {
                hit = true;
                closest_lambda = temp_intersection.lambda();
                closest_intersection = temp_intersection;
            }
        }
    }
    intersection = closest_intersection;
    return hit;
}

bool scene::any_intersection(const ray &ray) const
{
    for (auto const &object : objects_)
    {
        if (object->intersects_any(ray)) return true;
    }
    return false;
}
