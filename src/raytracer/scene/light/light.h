#pragma once

#include "math/vector3.h"

class light
{
public:
    explicit light(
            const math::vector3d &position = math::vector3d(0.0, 0.0, 0.0),
            const math::vector3d &spectral_intensity = math::vector3d(1.0, 1.0, 1.0));

    const math::vector3d &position() const
    { return position_; }

    const math::vector3d &spectral_intensity() const
    { return spectral_intensity_; }

    void set_position(const math::vector3d &position)
    { position_ = position; }

    void set_spectral_intensity(const math::vector3d &spectral_intensity)
    { spectral_intensity_ = spectral_intensity; }


private:
    math::vector3d position_;
    math::vector3d spectral_intensity_;
};
