#include "light.h"


light::light(const math::vector3d &position,
             const math::vector3d &spectral_intensity) : position_(
        position), spectral_intensity_(spectral_intensity)
{
}
