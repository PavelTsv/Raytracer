#pragma once

#include "object.h"

class plane : public object
{
public:
    plane(const math::vector3d &point_on_plane,
          const math::vector3d &normal);

    bool
    intersects(const ray &ray, ray_intersection &intersection) override;

    bool
    intersects_any(const ray &ray) override;

private:
    math::vector3d point_on_plane_;
    math::vector3d normal_;
};
