#include <cmath>
#include "plane.h"
#include "math/math_functions.h"

plane::plane(const math::vector3d &point_on_plane,
             const math::vector3d &normal) : point_on_plane_(
        point_on_plane), normal_(normal)
{
}

bool plane::intersects(const ray &ray,
                       ray_intersection &intersection)
{
    const auto denom = normal_.dot(ray.direction());
    if (std::abs(denom) > math::SAFETY_EPS)
    {
        const auto t =
                (point_on_plane_ - ray.origin()).dot(normal_) / denom;
        if (t >= 0)
        {
            intersection = ray_intersection(ray, shared_from_this(), t,
                                            normal_);
            return true;
        }
    }

    return false;
}

bool plane::intersects_any(const ray &ray)
{
    const auto denom = normal_.dot(ray.direction());
    if (std::abs(denom) > math::SAFETY_EPS)
    {
        const auto t =
                (point_on_plane_ - ray.origin()).dot(normal_) / denom;
        if (t >= 0) return true;
    }
}
