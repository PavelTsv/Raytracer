#pragma once

#include "math/vector3.h"
#include "raytracer/ray.h"
#include "raytracer/ray_intersection.h"

    class sphere : public object
    {
    public:
        sphere(const math::vector3d &center, const double radius);

        bool
        intersects(const ray &ray, ray_intersection &intersection) override;

        bool intersects_any(const ray &ray) override;

    private:
        math::vector3d center_;
        double radius_;
        double radius_power_of_two;
    };
