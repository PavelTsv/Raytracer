#include "sphere.h"
#include "math/math_functions.h"

sphere::sphere(const math::vector3d &center, const double radius)
        : center_(center), radius_(radius),
          radius_power_of_two(radius * radius)
{
}

bool sphere::intersects(const ray &ray, ray_intersection &intersection)
{
    auto length_to_center = ray.origin() - center_;
    const auto a = ray.direction().dot(ray.direction());
    const auto b = 2 * ray.direction().dot(length_to_center);
    const auto c = length_to_center.dot(length_to_center) -
                   radius_power_of_two;

    double t0, t1;

    if (!math::solve_quadratic(a, b, c, t0, t1)) return false;

    if (t0 > t1) math::swap(t0, t1);

    if (t0 < 0)
    {
        t0 = t1;
        if (t0 < t1) return false;
    }
    intersection = ray_intersection(ray, shared_from_this(), t0,
                                    ray.point_on_ray(t0));
    return true;
}

bool sphere::intersects_any(const ray &ray)
{
    auto length_to_center = ray.origin() - center_;
    const auto a = ray.direction().dot(ray.direction());
    const auto b = 2 * ray.direction().dot(length_to_center);
    const auto c = length_to_center.dot(length_to_center) -
                   radius_power_of_two;

    auto disc = b * b - 4 * a * c;

    return disc > 0;
}

