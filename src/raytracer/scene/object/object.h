#pragma once

#include "raytracer/ray.h"
#include "raytracer/ray_intersection.h"
#include "raytracer/material/material.h"

class ray_intersection;

class material;

class object : public std::enable_shared_from_this<object>
{
public:
    object()= default;

    virtual ~object() = default;

    virtual bool
    intersects(const ray &ray, ray_intersection &intersection) = 0;

    virtual bool intersects_any(const ray &ray) = 0;

    void set_surface(const std::shared_ptr<material> new_material)
    { material_ = new_material; }

    std::shared_ptr<const material> get_surface() const
    { return material_; }

private:
    std::shared_ptr<material> material_;
};
