#pragma once

#include <cstddef>
#include "math/vector3.h"
#include "raytracer/ray.h"
#include "math/math_functions.h"

class camera
{
public:
    camera();

    ~camera();

    ray primary_ray(const size_t x, const size_t y) const;

    void set_resolution(const size_t width, const size_t height)
    {
        width_ = width;
        height_ = height;
        calculate_image_vectors();
    }

    void
    set_fov(const double horizontal_fov_deg, const double vertical_fov_deg)
    {
        horizontal_fov_ = horizontal_fov_deg;
        vertical_fov_ = vertical_fov_deg;
        calculate_image_vectors();
    }

    void set_position(const math::vector3d &position)
    {
        position_ = position;
        calculate_image_vectors();
    }

    void set_look_at(const math::vector3d &look_at)
    {
        loot_at_ = look_at;
        calculate_image_vectors();
    }

    void set_up(const math::vector3d &up)
    {
        up_ = up;
    }

    const math::vector3d &position() const
    { return position_; }

    const math::vector3d &look_at() const
    { return loot_at_; }

    const math::vector3d &up() const
    { return up_; }

    double horizontal_fov() const
    { return horizontal_fov_; }

    double vertical_fov() const
    { return vertical_fov_; }

    size_t width() const
    { return width_; }

    size_t height() const
    { return height_; }

    const math::vector3d &direction() const
    { return direction_; }

    const math::vector3d &down() const
    { return down_; }

    const math::vector3d &right() const
    { return right_; }

    const math::vector3d &top_left() const
    { return top_left_; }


private:
    math::vector3d position_;
    math::vector3d up_;
    math::vector3d loot_at_;

    math::vector3d direction_;
    math::vector3d down_;
    math::vector3d right_;
    math::vector3d top_left_;

    size_t width_;
    size_t height_;
    double horizontal_fov_;
    double vertical_fov_;


    void calculate_image_vectors();
};
