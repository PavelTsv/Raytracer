#include <cstddef>
#include "camera.h"

camera::camera() : position_(1.0, 1.0, 1.0), up_(0.0, 1.0, 0.0),
                   loot_at_(0.0, 0.0, 0.0),
                   width_(640), height_(480), horizontal_fov_(90),
                   vertical_fov_(90)
{
    calculate_image_vectors();
}

camera::~camera() = default;

ray camera::primary_ray(const size_t x, const size_t y) const
{
    return ray{position_,
               top_left_ + right_ * double(x) - down_ * double(y) -
               position_};
}

void camera::calculate_image_vectors()
{
    direction_ = (loot_at_ - position_).normalize();

    right_ = direction_.cross(up_).normalize();
    down_ = direction_.cross(right_).normalize();

    const auto aspect_ration = double(width_) / height_;
    right_ *= tan(horizontal_fov_ * math::PI / 360.0) * aspect_ration;
    down_ *= tan(vertical_fov_ * math::PI / 360.0);

    top_left_ = position_ + direction_ - right_ + down_;

    right_ *= 2.0 / width_;
    down_ *= 2.0 / height_;
}
