#pragma once

#include "material.h"

class diffuse_surface : public material
{
public:
    explicit diffuse_surface(const math::vector4d &color);

    math::vector4d shade(const ray_intersection &ray_intersection,
                   const light &light) const override;
};