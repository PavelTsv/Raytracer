#pragma once

#include "math/vector4.h"
#include "material.h"

class const_color_surface : public material
{
public:
    explicit const_color_surface(const math::vector4d &color);

    math::vector4d shade(const ray_intersection &ray_intersection,
                   const light &light) const override;
};
