#pragma once

#include "math/vector4.h"
#include "raytracer/ray_intersection.h"
#include "raytracer/scene/light/light.h"

class ray_intersection;

class material
{
public:
    explicit material(
            const math::vector4d &color = math::vector4d(0.3, 0.3, 0.3, 1.0),
            const double reflection = 0.0) : color_{color},
                                             reflection_(reflection)
    {
    }

    virtual ~material() = default;

    virtual math::vector4d shade(const ray_intersection &ray_intersection,
          const light &light) const = 0;

    const math::vector4d &color() const
    {
        return color_;
    }

    double reflection() const
    {
        return reflection_;
    }

private:
    math::vector4d color_;
    double reflection_;
};
