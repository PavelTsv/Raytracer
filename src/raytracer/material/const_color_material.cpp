#include "const_color_material.h"

const_color_surface::const_color_surface(const math::vector4d &color)
        : material(color)
{
}

math::vector4d
const_color_surface::shade(const ray_intersection &ray_intersection,
                           const light &light) const
{
    return this->color();
}
