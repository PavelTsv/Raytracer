#include <algorithm>
#include "diffuse_material.h"

diffuse_surface::diffuse_surface(const math::vector4d &color) : material(
        color)
{
}

math::vector4d diffuse_surface::shade(
        const ray_intersection &ray_intersection,
        const light &light) const
{
    auto N = ray_intersection.normal();
    auto L = (light.position() -
              ray_intersection.position()).normalize();

    const auto cosNL = std::max(N.dot(L), double(0));

    return color() * cosNL;
}
