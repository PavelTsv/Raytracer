#pragma once

#include "math/vector3.h"

class ray
{
public:
    ray() = default;

    ray(const math::vector3d &origin, const math::vector3d &direction)
            : origin_{origin}, direction_{direction}
    {
    }

   math::vector3d point_on_ray(const double lambda) const
    { return origin_ + direction_ * lambda; }

    const math::vector3d &origin() const
    { return origin_; }

    const math::vector3d &direction() const
    { return direction_; }

private:
    math::vector3d origin_;
    math::vector3d direction_;
};
