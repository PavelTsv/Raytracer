#pragma once

#include <vector>
#include <cstddef>
#include <string>
#include "math/vector4.h"

class image
{
public:
    image();

    image(const size_t width, const size_t height);

    ~image();

    bool save_to_tga(const std::string &filename) const;

    void set_pixel(math::vector4d &color, const size_t x, const size_t y)
    { data_[x + width_ * y] = color; }

    const math::vector4d &pixel(const size_t x, const size_t y) const
    { return data_[x + width_ * y]; }

    const std::vector<math::vector4d> &data() const
    { return data_; };

    size_t width() const
    { return width_; }

    size_t height() const
    { return height_; }

private:
    size_t width_;
    size_t height_;
    std::vector<math::vector4d> data_;

    void update_vector_size();
};
