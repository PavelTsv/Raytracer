
#include <algorithm>
#include <string>
#include <fstream>
#include <iostream>
#include "image.h"
#include "math/math_functions.h"

image::image() : width_(0), height_(0)
{
}

image::image(const size_t width, const size_t height) : width_(width),
                                                        height_(height)
{
    update_vector_size();
}

image::~image() = default;

bool image::save_to_tga(const std::string &filename) const
{
    std::string suffix(".tga"), output_name(filename), output_name_lower(
            filename);

    std::transform(output_name_lower.begin(),
                   output_name_lower.end(),
                   output_name_lower.begin(),
                   ::tolower);

    if (!std::equal(suffix.rbegin(),
                    suffix.rend(),
                    output_name_lower.rbegin()))
        output_name += suffix;

    std::ofstream f(output_name, std::ios::binary | std::ios::out);

    if (!f.is_open())
    {
        std::cerr << "image::save_to_tga: could not open file." <<
                  output_name << std::endl;
        return false;
    }

    if (data_.empty())
    {
        std::cerr << "image::save_to_tga: image data uninitialized."
                  << std::endl;
        return false;
    }

    if (width_ < 1 || height_ < 1)
    {
        std::cerr
                << "Iimage::save_to_tga: image dimensions too small to write ("
                << width_ << "," << height_ << ")" << std::endl;
        return false;
    }


    f.put(0);
    f.put(0);
    f.put(2);
    f.put(0);
    f.put(0);
    f.put(0);
    f.put(0);
    f.put(0);
    f.put(0);
    f.put(0);
    f.put(0);
    f.put(0);
    f.put(static_cast<char>(width_ & 0x00FF));
    f.put(static_cast<unsigned char>((width_ & 0xFF00) / 256));
    f.put(static_cast<unsigned char>(height_ & 0x00FF));
    f.put(static_cast<unsigned char>((height_ & 0xFF00) / 256));
    f.put(32);
    f.put(0);

    for (size_t i = 0; i < height_ * width_; i++)
    {
        f.put(static_cast<unsigned char>(math::clamp(data_[i][2]) * 255));
        f.put(static_cast<unsigned char>(math::clamp(data_[i][1]) * 255));
        f.put(static_cast<unsigned char>(math::clamp(data_[i][0]) * 255));
        f.put(static_cast<unsigned char>(math::clamp(data_[i][3]) * 255));
    }

    f.close();

    return true;
}

void image::update_vector_size()
{
    data_.clear();
    data_.resize(width_ * height_);
}
