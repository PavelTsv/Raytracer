#include "raytracer.h"


raytracer::raytracer() = default;
raytracer::~raytracer() = default;

math::vector4d raytracer::trace(const ray &ray) const
{
    ray_intersection intersection;
    if (scene_->closest_intersection(ray, intersection))
    {
        return this->shade(intersection);
    }
    return scene_->background_color();
}

math::vector4d raytracer::shade(const ray_intersection &intersection) const
{
    const auto offset(intersection.normal() * math::SAFETY_EPS);

    math::vector4d color(0.0, 0.0, 0.0, 1.0);

    for (auto i = 0; i < scene_->lights().size(); i++)
    {
        const auto light = *scene_->lights()[i].get();

        const auto l = intersection.position() + offset - light.position();
        const ray shadow_ray(light.position(), l);

        if (scene_->any_intersection(shadow_ray))
        {
            color += intersection.get_object()->get_surface()->shade(
                    intersection, light);
        }

    }
    return color;
}
